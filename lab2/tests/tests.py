import pytest

from tests.testProcess import read_line, write_line, get_process

program_under_test = './../cmake-build-debug/lab2'
program_args = []
second = 1
action_timeout = 0.5 * second


async def get_readable_format_time(hours_24_format: int, minutes: int) -> str:
    process = await get_process(program_under_test, *program_args)
    line = await read_line(process.stdout, action_timeout)
    welcome_message = line.decode("utf-8")
    write_line(process.stdin, f'{hours_24_format} {minutes}\n'.encode("utf-8"))
    line = await read_line(process.stdout, action_timeout)
    received = line.decode("utf-8")
    return received


@pytest.mark.asyncio
async def test_night_time_02_48():
    expected = '2 часа 48 минут ночи'
    received = await get_readable_format_time(2, 48)
    assert received == expected


@pytest.mark.asyncio
async def test_morning_time_05_00():
    expected = '5 часов утра ровно'
    received = await get_readable_format_time(5, 0)
    assert received == expected


@pytest.mark.asyncio
async def test_midday_time():
    expected = 'полдень'
    received = await get_readable_format_time(12, 0)
    assert received == expected


@pytest.mark.asyncio
async def test_day_time_12_23():
    expected = '12 часов 23 минуты дня'
    received = await get_readable_format_time(12, 23)
    assert received == expected


@pytest.mark.asyncio
async def test_day_time_13_13():
    expected = '1 час 13 минут дня'
    received = await get_readable_format_time(13, 13)
    assert received == expected


@pytest.mark.asyncio
async def test_evening_time_9_33():
    expected = '9 часов 33 минуты вечера'
    received = await get_readable_format_time(21, 33)
    assert received == expected


@pytest.mark.asyncio
async def test_midnight_time_0_0():
    expected = 'полночь'
    received = await get_readable_format_time(0, 0)
    assert received == expected


@pytest.mark.asyncio
async def test_night_time_03_01():
    expected = '3 часа 1 минута ночи'
    received = await get_readable_format_time(3, 1)
    assert received == expected


@pytest.mark.asyncio
async def test_day_time_13_21():
    expected = '1 час 21 минута дня'
    received = await get_readable_format_time(13, 21)
    assert received == expected


@pytest.mark.asyncio
async def test_day_time_14_24():
    expected = '2 часа 24 минуты дня'
    received = await get_readable_format_time(14, 24)
    assert received == expected


@pytest.mark.asyncio
async def test_evening_time_20_20():
    expected = '8 часов 20 минут вечера'
    received = await get_readable_format_time(20, 20)
    assert received == expected
