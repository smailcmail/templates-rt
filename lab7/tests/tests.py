import os

import pytest
from subprocess import STDOUT, check_output

work_directory = "./../cmake-build-debug"
program_under_test = './lab7'
program_args = []
second = 1
action_timeout = 10 * second


@pytest.mark.asyncio
async def test():
    cmd = [program_under_test]
    os.chdir(work_directory)
    output = check_output(cmd, stderr=STDOUT, timeout=action_timeout)
    result = output.decode("utf-8")
    print("\nКонсольный вывод:")
    print(result)

    assert True
