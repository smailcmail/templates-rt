# Лабораторная работа №3

Задание [здесь](https://gitlab.com/iu5edu/cpp-course-sem1/textbook/-/blob/main/website/docs/labs/lab3/intro.md).

Решения:

- задания №1 [сюда](../lab3_1/README.md)
- задания №2 [сюда](../lab3_2/README.md)
- задания №4 [сюда](../lab3_3/README.md)
- задания №5 [сюда](../lab3_4/README.md)
